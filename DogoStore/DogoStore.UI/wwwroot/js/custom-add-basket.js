﻿$(document).on('click', '#addToCard', function () {

    var productId = $("#hdnProductId").val();
    var quantity = $("#select_quantity option:selected").val();

    $.ajax({
        type: 'post',
        url: '/Basket/AddToBasket?ProductId=' + productId + '&Quantity=' + quantity,
        success: function (data) {
            if (data) {
                window.location = '/Basket/Index';
            }
            else {
                swal('Hata Oluştu!', 'İşlem sırasında hata oluştu?', 'warning');
                return false;
            }
        }
    });

});