﻿$(document).on('click', '.removeProduct', function () {

    var id = $(this).data("id");

    swal({
        title: 'Ürün Silinecek!',
        text: "Silmek istediğinize emin misiniz?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet, sil!',
        cancelButtonText: 'İptal!',
        closeOnConfirm: false
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: 'post',
                url: '/Product/Remove?Id=' + id,
                success: function (data) {
                    if (data) {
                        swal('Silindi!', 'Ürün silindi?', 'success');
                        $("#tr_" + id).fadeOut($("#tr_" + id).remove());
                    }
                    else {
                        swal('Hata Oluştu!', 'İşlem sırasında hata oluştu?', 'warning');
                        return false;
                    }
                }
            });

        }
    });
});

$(document).on('click', '.removeBasketItem', function () {

    var id = $(this).data("id");

    swal({
        title: 'Sepet\'ten Silinecek!',
        text: "Sepetten istediğinize emin misiniz?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Evet, sil!',
        cancelButtonText: 'İptal!',
        closeOnConfirm: false
    }).then(function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                type: 'post',
                url: '/Basket/Remove?Id=' + id,
                success: function (data) {
                    if (data) {
                        swal('Silindi!', 'Ürün silindi?', 'success');
                        $("#tr_" + id).fadeOut($("#tr_" + id).remove());
                    }
                    else {
                        swal('Hata Oluştu!', 'İşlem sırasında hata oluştu?', 'warning');
                        return false;
                    }
                }
            });

        }
    });
});