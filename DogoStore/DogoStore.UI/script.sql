USE [DogoStore]
GO
/****** Object:  User [dogo_user]    Script Date: 17.08.2020 05:02:08 ******/
CREATE USER [dogo_user] FOR LOGIN [dogo_user] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [dogo_user]
GO
ALTER ROLE [db_datareader] ADD MEMBER [dogo_user]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [dogo_user]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Addresses]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Addresses](
	[AddressId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[AddressName] [nvarchar](50) NOT NULL,
	[AddressDesc] [nvarchar](150) NOT NULL,
	[AddressDetail] [nvarchar](250) NOT NULL,
	[AddressCity] [nvarchar](50) NOT NULL,
	[AddressCountry] [nvarchar](50) NOT NULL,
	[AddressCounty] [nvarchar](50) NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Addresses] PRIMARY KEY CLUSTERED 
(
	[AddressId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BasketItems]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BasketItems](
	[BasketItemId] [int] IDENTITY(1,1) NOT NULL,
	[BasketId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Price] [decimal](18, 2) NULL,
	[Vat] [decimal](18, 2) NULL,
	[CreateTime] [datetime] NULL,
	[Amount] [int] NULL,
	[TotalPrice] [decimal](18, 2) NULL,
 CONSTRAINT [PK_BasketItems] PRIMARY KEY CLUSTERED 
(
	[BasketItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Baskets]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Baskets](
	[BasketId] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeId] [int] NOT NULL,
	[AddressId] [int] NOT NULL,
	[StateId] [int] NOT NULL,
	[TotalAmount] [decimal](18, 2) NOT NULL,
	[TotalVat] [decimal](18, 2) NOT NULL,
	[SubTotal] [decimal](18, 2) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[UserId] [nvarchar](256) NULL,
	[OrderDate] [datetime] NULL,
 CONSTRAINT [PK_Baskets] PRIMARY KEY CLUSTERED 
(
	[BasketId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BasketStates]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BasketStates](
	[StateId] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [nvarchar](50) NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_BasketStates] PRIMARY KEY CLUSTERED 
(
	[StateId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[CategoryName] [nvarchar](150) NOT NULL,
	[CategorySort] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PayTypes]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PayTypes](
	[PayTypeId] [int] IDENTITY(1,1) NOT NULL,
	[PayTypeName] [nvarchar](50) NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_PayTypes] PRIMARY KEY CLUSTERED 
(
	[PayTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Products]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Products](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[ProductCategoryId] [int] NOT NULL,
	[ProductName] [nvarchar](150) NOT NULL,
	[ProductPrice] [decimal](18, 2) NOT NULL,
	[ProductStock] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[IsDelete] [bit] NOT NULL,
	[ProductStockCode] [nvarchar](50) NULL,
	[IsHome] [bit] NULL,
 CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200814163303_initialcreate', N'3.1.7')
SET IDENTITY_INSERT [dbo].[Addresses] ON 

INSERT [dbo].[Addresses] ([AddressId], [UserId], [AddressName], [AddressDesc], [AddressDetail], [AddressCity], [AddressCountry], [AddressCounty], [IsDelete]) VALUES (1, 1, N'İş', N'1', N'1', N'1', N'1', N'1', 0)
SET IDENTITY_INSERT [dbo].[Addresses] OFF
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount]) VALUES (N'128a74be-1051-4515-952d-2a1cbb2f7c76', N'otryki', N'OTRYKI', N'okantiryaki@outlook.com', N'OKANTIRYAKI@OUTLOOK.COM', 0, N'AQAAAAEAACcQAAAAEBfPvrXwOCD0HasE6yc4pQ9f+hME2KTLBLZJ1qEDzuAy1xBQxKqUDmATBxuROA/ZZw==', N'RYNLINUEAPDJCMXZ7HXM572ZDFZJYBUL', N'a6cd8594-979d-42a1-b1a7-e69c0193fbea', NULL, 0, 0, NULL, 1, 0)
SET IDENTITY_INSERT [dbo].[BasketItems] ON 

INSERT [dbo].[BasketItems] ([BasketItemId], [BasketId], [ProductId], [Price], [Vat], [CreateTime], [Amount], [TotalPrice]) VALUES (34, 33, 288, CAST(12.00 AS Decimal(18, 2)), CAST(2.16 AS Decimal(18, 2)), CAST(N'2020-08-17T04:44:30.977' AS DateTime), 1, CAST(12.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[BasketItems] OFF
SET IDENTITY_INSERT [dbo].[Baskets] ON 

INSERT [dbo].[Baskets] ([BasketId], [PayTypeId], [AddressId], [StateId], [TotalAmount], [TotalVat], [SubTotal], [CreateDate], [LastUpdate], [IsActive], [UserId], [OrderDate]) VALUES (33, 0, 0, 1, CAST(12.00 AS Decimal(18, 2)), CAST(2.16 AS Decimal(18, 2)), CAST(9.84 AS Decimal(18, 2)), CAST(N'2020-08-17T04:44:27.920' AS DateTime), CAST(N'2020-08-17T04:44:31.537' AS DateTime), 1, N'128a74be-1051-4515-952d-2a1cbb2f7c76', NULL)
INSERT [dbo].[Baskets] ([BasketId], [PayTypeId], [AddressId], [StateId], [TotalAmount], [TotalVat], [SubTotal], [CreateDate], [LastUpdate], [IsActive], [UserId], [OrderDate]) VALUES (34, 0, 0, 1, CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(N'2020-08-17T04:52:20.853' AS DateTime), CAST(N'2020-08-17T04:52:20.857' AS DateTime), 1, N'128a74be-1051-4515-952d-2a1cbb2f7c76', NULL)
SET IDENTITY_INSERT [dbo].[Baskets] OFF
SET IDENTITY_INSERT [dbo].[BasketStates] ON 

INSERT [dbo].[BasketStates] ([StateId], [StateName], [IsDelete]) VALUES (1, N'İşlemde', 0)
INSERT [dbo].[BasketStates] ([StateId], [StateName], [IsDelete]) VALUES (2, N'Onay Bekliyor', 0)
INSERT [dbo].[BasketStates] ([StateId], [StateName], [IsDelete]) VALUES (3, N'Onaylandı', 0)
INSERT [dbo].[BasketStates] ([StateId], [StateName], [IsDelete]) VALUES (4, N'İptal', 0)
SET IDENTITY_INSERT [dbo].[BasketStates] OFF
SET IDENTITY_INSERT [dbo].[Categories] ON 

INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [CategorySort], [CreateDate], [IsDelete]) VALUES (1, N'Gömlek', 1, CAST(N'2020-08-13T00:00:00.000' AS DateTime), 0)
INSERT [dbo].[Categories] ([CategoryId], [CategoryName], [CategorySort], [CreateDate], [IsDelete]) VALUES (2, N'Elbise', 1, CAST(N'2020-08-13T00:00:00.000' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Categories] OFF
SET IDENTITY_INSERT [dbo].[PayTypes] ON 

INSERT [dbo].[PayTypes] ([PayTypeId], [PayTypeName], [IsDelete]) VALUES (1, N'Kredi Kartı', 0)
INSERT [dbo].[PayTypes] ([PayTypeId], [PayTypeName], [IsDelete]) VALUES (2, N'Nakit', 0)
INSERT [dbo].[PayTypes] ([PayTypeId], [PayTypeName], [IsDelete]) VALUES (3, N'Kapıda ', 0)
SET IDENTITY_INSERT [dbo].[PayTypes] OFF
SET IDENTITY_INSERT [dbo].[Products] ON 

INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (160, 1, N'Deneme Ürün Gömlek 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 1)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (161, 1, N'Deneme Ürün Gömlek 2', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 1)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (162, 1, N'Deneme Ürün Gömlek 3', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 1)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (163, 1, N'Deneme Ürün Gömlek 4', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 1)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (164, 1, N'Deneme Ürün Gömlek 5', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 1)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (165, 1, N'Deneme Ürün Gömlek 6', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (166, 1, N'Deneme Ürün Gömlek 7', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (167, 1, N'Deneme Ürün Elbise 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (168, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (169, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (170, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (171, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (172, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (173, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (174, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (175, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (176, 1, N'Deneme Ürün Gömlek 1', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (177, 1, N'Deneme Ürün Gömlek 2', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (178, 1, N'Deneme Ürün Gömlek 3', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (179, 1, N'Deneme Ürün Gömlek 4', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (180, 1, N'Deneme Ürün Gömlek 5', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (181, 1, N'Deneme Ürün Gömlek 6', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (182, 1, N'Deneme Ürün Gömlek 7', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (183, 1, N'Deneme Ürün Elbise 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (184, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (185, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (186, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (187, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (188, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (189, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (190, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (191, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (192, 1, N'Deneme Ürün Gömlek 1', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (193, 1, N'Deneme Ürün Gömlek 2', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (194, 1, N'Deneme Ürün Gömlek 3', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (195, 1, N'Deneme Ürün Gömlek 4', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (196, 1, N'Deneme Ürün Gömlek 5', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (197, 1, N'Deneme Ürün Gömlek 6', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (198, 1, N'Deneme Ürün Gömlek 7', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (199, 1, N'Deneme Ürün Elbise 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (200, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (201, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (202, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (203, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (204, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (205, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (206, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (207, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (208, 1, N'Deneme Ürün Gömlek 1', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (209, 1, N'Deneme Ürün Gömlek 2', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (210, 1, N'Deneme Ürün Gömlek 3', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (211, 1, N'Deneme Ürün Gömlek 4', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (212, 1, N'Deneme Ürün Gömlek 5', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (213, 1, N'Deneme Ürün Gömlek 6', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (214, 1, N'Deneme Ürün Gömlek 7', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (215, 1, N'Deneme Ürün Elbise 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (216, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (217, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (218, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (219, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (220, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (221, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (222, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (223, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (224, 1, N'Deneme Ürün Gömlek 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (225, 1, N'Deneme Ürün Gömlek 2', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (226, 1, N'Deneme Ürün Gömlek 3', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (227, 1, N'Deneme Ürün Gömlek 4', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (228, 1, N'Deneme Ürün Gömlek 5', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (229, 1, N'Deneme Ürün Gömlek 6', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (230, 1, N'Deneme Ürün Gömlek 7', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (231, 1, N'Deneme Ürün Elbise 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (232, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (233, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (234, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (235, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (236, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (237, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (238, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (239, 2, N'Deneme Ürün 1', CAST(20.00 AS Decimal(18, 2)), 250, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (240, 1, N'Deneme Ürün Gömlek 1', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (241, 1, N'Deneme Ürün Gömlek 2', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (242, 1, N'Deneme Ürün Gömlek 3', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (243, 1, N'Deneme Ürün Gömlek 4', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (244, 1, N'Deneme Ürün Gömlek 5', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (245, 1, N'Deneme Ürün Gömlek 6', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (246, 1, N'Deneme Ürün Gömlek 7', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (247, 1, N'Deneme Ürün Elbise 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (248, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (249, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (250, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (251, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (252, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (253, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (254, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (255, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (256, 1, N'Deneme Ürün Gömlek 1', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (257, 1, N'Deneme Ürün Gömlek 2', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (258, 1, N'Deneme Ürün Gömlek 3', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 0)
GO
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (259, 1, N'Deneme Ürün Gömlek 4', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (260, 1, N'Deneme Ürün Gömlek 5', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (261, 1, N'Deneme Ürün Gömlek 6', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (262, 1, N'Deneme Ürün Gömlek 7', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (263, 1, N'Deneme Ürün Elbise 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (264, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (265, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (266, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (267, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (268, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (269, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (270, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (271, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (272, 1, N'Deneme Ürün Gömlek 1', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode1', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (273, 1, N'Deneme Ürün Gömlek 2', CAST(50.00 AS Decimal(18, 2)), 999, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode2', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (274, 1, N'Deneme Ürün Gömlek 3', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode3', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (275, 1, N'Deneme Ürün Gömlek 4', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode4', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (276, 1, N'Deneme Ürün Gömlek 5', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode5', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (277, 1, N'Deneme Ürün Gömlek 6', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode6', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (278, 1, N'Deneme Ürün Gömlek 7', CAST(50.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode7', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (279, 1, N'Deneme Ürün Elbise 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode8', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (280, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode9', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (281, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode10', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (282, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode11', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (283, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode12', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (284, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode13', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (285, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode14', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (286, 2, N'Deneme Ürün 1', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode15', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (287, 2, N'Deneme Ürün 5', CAST(1.00 AS Decimal(18, 2)), 1, CAST(N'2020-08-17T00:00:00.000' AS DateTime), 0, N'StockCode16', 0)
INSERT [dbo].[Products] ([ProductId], [ProductCategoryId], [ProductName], [ProductPrice], [ProductStock], [CreateDate], [IsDelete], [ProductStockCode], [IsHome]) VALUES (288, 2, N'Senin Testin', CAST(12.00 AS Decimal(18, 2)), 254, CAST(N'2020-08-17T04:44:14.930' AS DateTime), 0, N'StokCode', 1)
SET IDENTITY_INSERT [dbo].[Products] OFF
ALTER TABLE [dbo].[Addresses] ADD  CONSTRAINT [DF_Addresses_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[BasketStates] ADD  CONSTRAINT [DF_BasketStates_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[Categories] ADD  CONSTRAINT [DF_Categories_CategorySort]  DEFAULT ((9999)) FOR [CategorySort]
GO
ALTER TABLE [dbo].[Categories] ADD  CONSTRAINT [DF_Categories_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[PayTypes] ADD  CONSTRAINT [DF_PayTypes_IsDelete]  DEFAULT ((0)) FOR [IsDelete]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[BasketItems]  WITH CHECK ADD  CONSTRAINT [FK_BasketItems_Baskets] FOREIGN KEY([BasketId])
REFERENCES [dbo].[Baskets] ([BasketId])
GO
ALTER TABLE [dbo].[BasketItems] CHECK CONSTRAINT [FK_BasketItems_Baskets]
GO
ALTER TABLE [dbo].[BasketItems]  WITH CHECK ADD  CONSTRAINT [FK_BasketItems_Products] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Products] ([ProductId])
GO
ALTER TABLE [dbo].[BasketItems] CHECK CONSTRAINT [FK_BasketItems_Products]
GO
ALTER TABLE [dbo].[Baskets]  WITH CHECK ADD  CONSTRAINT [FK_Baskets_BasketStates] FOREIGN KEY([StateId])
REFERENCES [dbo].[BasketStates] ([StateId])
GO
ALTER TABLE [dbo].[Baskets] CHECK CONSTRAINT [FK_Baskets_BasketStates]
GO
ALTER TABLE [dbo].[Products]  WITH CHECK ADD  CONSTRAINT [FK_Products_Categories] FOREIGN KEY([ProductCategoryId])
REFERENCES [dbo].[Categories] ([CategoryId])
GO
ALTER TABLE [dbo].[Products] CHECK CONSTRAINT [FK_Products_Categories]
GO
/****** Object:  Trigger [dbo].[StockUpdate]    Script Date: 17.08.2020 05:02:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE trigger [dbo].[StockUpdate] on [dbo].[BasketItems]

AFTER INSERT,DELETE,UPDATE
AS
BEGIN
	  SET NOCOUNT ON;
      DECLARE @Action as char(1)
	  DECLARE @Count as int;
	  DECLARE @ProdId as int;
	  DECLARE @BasketItemId as int;
	  DECLARE @Amount as int;
	  DECLARE @NewAmount as int;

SET @Action = 'I'
SELECT @Count = COUNT(*) FROM DELETED

	if @Count > 0
		BEGIN
			SET @Action = 'D'
			SELECT @Count = COUNT(*) FROM INSERTED
			IF @Count > 0
				SET @Action = 'U'
		END
END

		IF @Action = 'I'
		BEGIN
			SELECT @ProdId = ProductId,@Amount = Amount FROM INSERTED
			update Products set ProductStock=ProductStock-@Amount where ProductId=@ProdId 
		END
        IF @Action = 'U'
		BEGIN
		select @Amount=Amount, @ProdId=ProductId from deleted
		select @NewAmount=Amount , @ProdId=ProductId from inserted
			
			IF(@NewAmount<@Amount)
				BEGIN
				update Products set ProductStock=ProductStock+(@Amount-@NewAmount) where ProductId=@ProdId 
				END
			ELSE
				BEGIN
				update Products set ProductStock=ProductStock-(@NewAmount-@Amount) where  ProductId=@ProdId 
				END

		END
        IF @Action = 'D'
		BEGIN
		select @Amount=Amount, @ProdId=ProductId from deleted
		update Products set ProductStock=ProductStock+@Amount where  ProductId=@ProdId
		END
GO
ALTER TABLE [dbo].[BasketItems] ENABLE TRIGGER [StockUpdate]
GO
