﻿using DogoStore.Entities.ViewModel.User;
using DogoStore.UI.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace DogoStore.UI.Controllers
{
    public class UserController : Controller
    {

        private UserManager<AppIdentityUser> _userManager;
        private SignInManager<AppIdentityUser> _signInmanager;

        public UserController(UserManager<AppIdentityUser> userManager, SignInManager<AppIdentityUser> signInmanager)
        {
            _userManager = userManager;
            _signInmanager = signInmanager;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
                return View(loginViewModel);

            var user = await _userManager.FindByNameAsync(loginViewModel.UserName);

            if (user == null)
                ModelState.AddModelError(String.Empty, "Kullanıcı adı veri tabanında bulunamadı.");
            
            var result = await _signInmanager.PasswordSignInAsync(loginViewModel.UserName, loginViewModel.Password, true, false);

            if (result.Succeeded)
                return RedirectToAction("Index", "Home");

            ModelState.AddModelError(String.Empty, "Giriş Başarısız");
            return View(loginViewModel);
        }

        public async Task<IActionResult> LogOut()
        {
            await _signInmanager.SignOutAsync();

            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> AccessDenied()
        {
            await _signInmanager.SignOutAsync();

            return View();
        }


        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel registerViewModel)
        {
            if (!ModelState.IsValid)
                return View(registerViewModel);

            var user = new AppIdentityUser
            {
                UserName = registerViewModel.UserName,
                Email = registerViewModel.Email
            };

            var result = await _userManager.CreateAsync(user, registerViewModel.Password);

            if (result.Succeeded)
                return RedirectToAction("Index", "Home");

            return View(registerViewModel);

        }
    }
}
