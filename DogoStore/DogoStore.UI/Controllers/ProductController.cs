﻿using DogoStore.Business.Abstract;
using DogoStore.Entities.ViewModel.Category;
using DogoStore.Entities.ViewModel.Product;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using X.PagedList;

namespace DogoStore.UI.Controllers
{
    [Authorize]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;

        public ProductController(IProductService productService, ICategoryService categoryService)
        {
            _productService = productService;
            _categoryService = categoryService;
        }

        public IActionResult Index(int page = 1)
        {
            int pageSize = 10;
            
            var productViewModels = _productService.ListProductsAll().ToPagedList(page,pageSize);

            return View(productViewModels);
        }

        public IActionResult Add()
        {
            ProductViewModel productViewModel = new ProductViewModel();
            productViewModel.Categories = _categoryService.ListSelectCategories();
            productViewModel.SelectedCategory = productViewModel.Categories.FirstOrDefault();

            return View(productViewModel);
        }

        [HttpPost]
        public IActionResult Add(ProductViewModel productView)
        {
            if (!ModelState.IsValid)
                return View(productView);

            productView.ProductCategoryId = productView.SelectedCategory.ProductCategoryId;

            _productService.Add(productView);

            return RedirectToAction("Index", "Product");
        }

        public IActionResult Edit(int Id)
        {
            ProductViewModel productViewModel = new ProductViewModel();

            productViewModel = _productService.GetProduct(Id);
            productViewModel.Categories = _categoryService.ListSelectCategories();
            productViewModel.SelectedCategory = productViewModel.Categories.FirstOrDefault(x => x.ProductCategoryId == productViewModel.ProductCategoryId);
            return View(productViewModel);
        }

        [HttpPost]
        public IActionResult Edit(ProductViewModel productView)
        {
            if (!ModelState.IsValid)
                return View(productView);

            _productService.Edit(productView);

            return RedirectToAction("Index", "Product");
        }
        public IActionResult Detail(int Id)
        {
            ProductViewModel productViewModel = new ProductViewModel();
            productViewModel = _productService.GetProduct(Id);
            return View(productViewModel);
        }

        [HttpPost]
        public JsonResult Remove(int Id)
        {
            bool data = _productService.Remove(Id);
            return Json(data);
        }

    }
}
