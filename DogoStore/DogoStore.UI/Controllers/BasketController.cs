﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using DogoStore.UI.Identity;
using DogoStore.Business.Abstract;
using Microsoft.AspNetCore.Http;
using DogoStore.Entities.ViewModel.Basket;
using DogoStore.Business.Helper.Enum;
using DogoStore.Entities.Params.Basket;

namespace DogoStore.UI.Controllers
{
    [Authorize]
    public class BasketController : Controller
    {
        private UserManager<AppIdentityUser> _userManager;
        private readonly IBasketItemService _basketItemService;
        private readonly IBasketService _basketService;
        private readonly IProductService _productService;
        public BasketController(UserManager<AppIdentityUser> userManager, IBasketItemService basketItemService, IBasketService basketService, IProductService productService)
        {
            _userManager = userManager;
            _basketItemService = basketItemService;
            _basketService = basketService;
            _productService = productService;
        }
        public IActionResult Index()
        {
            var basketId = HttpContext.Session.GetInt32("basketId");
            BasketViewModel basketView = new BasketViewModel();
            basketView.BasketItemViewModels = new List<BasketItemViewModel>();

            if (basketId != null)
                return View(_basketService.GetBasketAndItems((int)basketId));

            var activeUserId = _userManager.GetUserId(this.User);
            basketId = _basketService.CreateBasketDefault(activeUserId, (int)BasketEnum.InProggress, (int)BasketActive.Active, 0);

            if (basketId > 0)
                HttpContext.Session.SetInt32("basketId", (int)basketId);

            return View(basketView);
        }

        public IActionResult Pay()
        {
            var basketId = HttpContext.Session.GetInt32("basketId");
            BasketViewModel basketView = _basketService.GetBasketAndItems((int)basketId);

            return View(basketView);
        }

        public IActionResult PaySuccess()
        {
            var basketId = HttpContext.Session.GetInt32("basketId");
            var res2 = _basketService.UpdateActive((int)basketId, (int)BasketActive.NotActive);
            var activeUserName = _userManager.GetUserName(this.User);

            if (res2 <= 0)
                return RedirectToAction("PayError", "Basket");

            PaySuccessViewModel paySuccessViewModel = new PaySuccessViewModel()
            {
                BasketId = (int)basketId,
                UserName = activeUserName,
                PayTypeName = "Nakit"
            };

            HttpContext.Session.Clear();
            return View(paySuccessViewModel);
        }

        [HttpPost]
        public IActionResult QuickPay()
        {
            var activeUserId = _userManager.GetUserId(this.User);
            var basketId = HttpContext.Session.GetInt32("basketId");
            string page = "PayError";

            var res1 = _basketService.UpdateState((int)basketId, (int)BasketEnum.Approved);

            if (res1 > 0)
                page = "PaySuccess";

            return RedirectToAction(page, "Basket");
        }

        public IActionResult Result()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddToBasket(int ProductId, int Quantity)
        {
            var activeUserId = _userManager.GetUserId(this.User);
            var basketId = HttpContext.Session.GetInt32("basketId");
            decimal price = _productService.GetProduct(ProductId).ProductPrice;

            if (basketId == null)
                basketId = _basketService.CreateBasketDefault(activeUserId, ProductId, Quantity, price);

            _basketItemService.AddToBasketWithId(new AddToBasketParam() { ActiveUserId = activeUserId, BasketId = (int)basketId, Price = price, ProductId = ProductId, Quantity = Quantity });

            HttpContext.Session.SetInt32("basketId", (int)basketId);

            return RedirectToAction("Index", "Basket");
        }

        [HttpPost]
        public JsonResult Remove(int Id)
        {
            bool data = _basketItemService.RemoveBasketItem(Id);
            return Json(data);
        }
    }
}
