﻿using DogoStore.DataAccess.Abstract;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Product;
using System.Collections.Generic;
using System.Data;

namespace DogoStore.DataAccess.Concrete
{
    internal class ProductRepository : RepositoryBase, IProductRepository
    {

        public ProductRepository(IDbTransaction transaction)
            : base(transaction)
        {


        }
        public int Add(Product entity)
        {
            return Execute(
                            sql: @"if not exists(select [ProductId] from [Products] where [ProductStockCode] = @ProductStockCode)
	                        insert into Products ([ProductCategoryId],[ProductName],[ProductPrice],[ProductStock],[CreateDate],[IsDelete],[IsHome],[ProductStockCode]) values(@ProductCategoryId,@ProductName,@ProductPrice,@ProductStock,@CreateDate,@IsDelete,@IsHome,@ProductStockCode)",

                            param: new { entity.ProductCategoryId, entity.ProductName, entity.ProductPrice, entity.ProductStock, entity.CreateDate, entity.IsDelete, entity.IsHome, entity.ProductStockCode }
                        );
        }

        public IEnumerable<Product> All()
        {
            return Query<Product>(
                sql: "SELECT * FROM Products"
            );
        }

        public Product Find(int ProductId)
        {
            return QuerySingleOrDefault<Product>(
               sql: @"SELECT * FROM [Products] where [ProductId] = @ProductId",
               param: new { ProductId }
           );
        }

        public ProductViewModel GetProduct(int ProductId)
        {
            return QuerySingleOrDefault<ProductViewModel>(
               sql: @"SELECT (select CategoryName from Categories where CategoryId = pr.ProductCategoryId) as CategoryName ,* FROM [Products] pr where [ProductId] = @ProductId",
               param: new { ProductId }
           );
        }

        public IEnumerable<ProductViewModel> HomePage()
        {
            return Query<ProductViewModel>(
                sql: @"SELECT *, (select CategoryName from Categories cg where cg.CategoryId = pr.ProductCategoryId) as  CategoryName FROM [Products] pr  where pr.[IsDelete] = 0  and pr.IsHome = 1 Order by ProductId desc"
            );
        }

        public IEnumerable<ProductViewModel> ListByCategoryProducts(int page, int categoryId)
        {
            return Query<ProductViewModel>(
                sql: @"
                    SELECT (ROW_NUMBER() OVER(ORDER BY pr.ProductId DESC)) + @countInPage * (@page - 1) AS [Key], * from
                    (SELECT  pr.ProductId, pr.ProductName,pr.ProductPrice,pr.ProductStock,pr.CreateDate,pr.IsDelete,pr.Home,pr.ProductStockCode,pc.CategoryName as CategoryName
                    FROM [Products] pr where pr.ProductCategoryId = @categoryId and where pr.[IsDelete] = 0
                    LEFT JOIN Categories pc on pr.ProductCategoryId = pc.CategoryId
                    ORDER BY ProductId DESC OFFSET (@page * @countInPage) ROWS FETCH NEXT @countInPage ROWS ONLY) pr",

                param: new { page, countInPage = 20, categoryId }
            );
        }

        public IEnumerable<ProductViewModel> ListProducts()
        {
            return Query<ProductViewModel>(
                sql: @"select pr.ProductId, pr.ProductName,pr.ProductPrice,pr.ProductStock,pr.CreateDate,pr.IsDelete,pr.IsHome,pr.ProductStockCode,pc.CategoryName as CategoryName FROM [Products] pr LEFT JOIN Categories pc on pr.ProductCategoryId = pc.CategoryId where pr.[IsDelete] = 0 ORDER BY pr.ProductId DESC"
            );
        }

        public int Remove(int ProductId)
        {
            return Execute(
                            sql: @"update Products set [IsDelete] = 1 where ProductId = @productId",
                            param: new { productId = ProductId }
                        );
        }

        public int Update(Product entity)
        {
            return Execute(
                            sql: @"update Products set [ProductCategoryId] = @productCategoryId,[ProductName] = @productName,
                                    [ProductPrice]=@productPrice,[ProductStock] = @productStock,[ProductStockCode] = @productStockCode,[IsHome] = @isHome where ProductId = @productId",

                            param: new
                            {
                                productStockCode = entity.ProductStockCode,
                                productCategoryId = entity.ProductCategoryId,
                                productName = entity.ProductName,
                                productPrice = entity.ProductPrice,
                                productStock = entity.ProductStock,
                                isHome = entity.IsHome,
                                productId = entity.ProductId
                            }
                        );
        }
    }
}
