﻿using DogoStore.DataAccess.Abstract;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Basket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DogoStore.DataAccess.Concrete
{
    public class BasketItemRepository : RepositoryBase, IBasketItemRepository
    {
        public BasketItemRepository(IDbTransaction transaction)
            : base(transaction)
        {

        }

        public int Add(BasketItem entity)
        {
            return Execute(
                            sql: @"insert into BasketItems([BasketId],[Price],[Vat],[CreateTime],[Amount],[TotalPrice],[ProductId]) values(@basketId,@price,@vat,@createTime,@amount,@totalPrice,@productId)",
                            param: new
                            {

                                basketId = entity.BasketId,
                                price = entity.Price,
                                vat = entity.Vat,
                                createTime = entity.CreateTime,
                                amount = entity.Amount,
                                totalPrice = entity.TotalPrice,
                                productId = entity.ProductId
                            }
                        );
        }

        public IEnumerable<BasketItem> All()
        {
            throw new NotImplementedException();
        }

        public int ChangeBasketItem(BasketItemViewModel entity)
        {
            return Execute(
                            sql: @"update BasketItems set Vat = @vat, Amount = @amount,TotalPrice = @totalPrice where BasketItemId = @basketItemId",
                            param: new
                            {
                                vat = entity.Vat,
                                amount = entity.Amount,
                                totalPrice = entity.TotalPrice,
                                basketItemId = entity.BasketItemId
                            }
                        );
        }

        public BasketItem Find(int key)
        {
            throw new NotImplementedException();
        }

        public BasketViewModel GetActiveBasket(string userId, int isActive, int stateId)
        {
            return QuerySingleOrDefault<BasketViewModel>(
               sql: @"SELECT [BasketId],[UserId],[PayTypeId],[AddressId],[StateId],[TotalAmount],[TotalVat] ,[SubTotal] ,[OrderDate],[CreateDate],[LastUpdate],[IsActive] FROM [Baskets] where IsActive = @isActive and  UserId = @userId and [StateId] = @stateId",
               param: new { isActive, userId, stateId }
           );
        }

        public int GetActiveBasketId(string userId, int isActive, int stateId)
        {
            return QuerySingleOrDefault<int>(
               sql: @"SELECT [BasketId] FROM [Baskets] where IsActive = @isActive and  UserId = @userId and [StateId] = @stateId",
               param: new { isActive, userId, stateId }
           );
        }

        public IEnumerable<BasketItem> GetBasketItems(int basketId)
        {
            return Query<BasketItem>(
               sql: @"SELECT bs.[BasketItemId],bs.[BasketId],bs.[ProductId],bs.[Price],bs.[Vat],bs.[CreateTime],bs.[Amount],bs.[TotalPrice] FROM [BasketItems] bs where bs.BasketId = @basketId",
               param: new { basketId });
        }

        public IEnumerable<BasketItemViewModel> GetListBasketItems(int basketId)
        {
            return Query<BasketItemViewModel>(
              sql: @"SELECT bs.[BasketItemId],(Select ProductName from Products pr where pr.ProductId = bs.ProductId) as ProductName,bs.[BasketId],bs.[ProductId],bs.[Price],bs.[Vat],bs.[CreateTime],bs.[Amount],bs.[TotalPrice] FROM [BasketItems] bs where bs.BasketId = @basketId",
              param: new { basketId });
        }

        public BasketItem IsSameProduct(int basketId, int productId)
        {
            return QuerySingleOrDefault<BasketItem>(
               sql: @"SELECT * FROM [BasketItems] where BasketId = @basketId and ProductId = @productId",
               param: new { basketId, productId }
           );
        }

        public int Remove(int basketItemId)
        {
            return Execute(
                           sql: @"delete from BasketItems where BasketItemId = @basketItemId",
                           param: new { basketItemId }
            );
        }

        public int Update(BasketItem entity)
        {
            throw new NotImplementedException();
        }
    }
}
