﻿using DogoStore.DataAccess.Abstract;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Basket;
using System.Collections.Generic;
using System.Data;

namespace DogoStore.DataAccess.Concrete
{
    public class BasketRepository : RepositoryBase, IBasketRepository
    {
        public BasketRepository(IDbTransaction transaction)
            : base(transaction)
        {

        }

        public int Add(Basket entity)
        {
            return Execute(
                                       sql: @"insert into Baskets ([UserId],[PayTypeId],[AddressId],[StateId],[TotalAmount],[TotalVat],[SubTotal],[OrderDate],[CreateDate],[LastUpdate],[IsActive]) values(@userId,@payTypeId,@addressId,@stateId,@totalAmount,@totalVat,@subTotal,@orderDate,@createDate,@lastUpdate,@isActive)",

                                       param: new
                                       {
                                           userId = entity.UserId,
                                           payTypeId = entity.PayTypeId,
                                           addressId = entity.AddressId,
                                           stateId = entity.StateId,
                                           totalAmount = entity.TotalAmount,
                                           totalVat = entity.TotalVat,
                                           subTotal = entity.SubTotal,
                                           orderDate = entity.OrderDate,
                                           createDate = entity.CreateDate,
                                           lastUpdate = entity.LastUpdate,
                                           isActive = entity.IsActive
                                       }
                                   );
        }
        public IEnumerable<Basket> All()
        {
            throw new System.NotImplementedException();
        }
        public int CreateBasket(BasketViewModel entity)
        {
            return ExecuteScalar<int>(
                                       sql: @"insert into Baskets ([UserId],[PayTypeId],[AddressId],[StateId],[TotalAmount],[TotalVat],[SubTotal],[CreateDate],[LastUpdate],[IsActive]) values(@userId,@payTypeId,@addressId,@stateId,@totalAmount,@totalVat,@subTotal,@createDate,@lastUpdate,@isActive); select SCOPE_IDENTITY();",

                                       param: new
                                       {
                                           userId = entity.UserId,
                                           payTypeId = entity.PayTypeId,
                                           addressId = entity.AddressId,
                                           stateId = entity.StateId,
                                           totalAmount = entity.TotalAmount,
                                           totalVat = entity.TotalVat,
                                           subTotal = entity.SubTotal,
                                           createDate = entity.CreateDate,
                                           lastUpdate = entity.LastUpdate,
                                           isActive = entity.IsActive
                                       }
                                   );
        }
        public Basket Find(string key)
        {
            throw new System.NotImplementedException();
        }
        public BasketViewModel GetBasket(int basketId)
        {
            return QuerySingleOrDefault<BasketViewModel>(
               sql: @"SELECT bs.[BasketId],us.UserName as UserName, bs.[UserId],bs.[PayTypeId],py.PayTypeName as PayTypeName,bs.[StateId],bst.[StateName] as [StateName],bs.[TotalAmount],bs.[TotalVat],bs.[SubTotal],bs.[OrderDate],bs.[CreateDate],bs.[LastUpdate],bs.[IsActive] FROM [Baskets] bs LEFT JOIN AspNetUsers us on us.Id = bs.UserId LEFT JOIN PayTypes py on py.PayTypeId = bs.PayTypeId LEFT JOIN BasketStates bst on bst.StateId = bs.StateId where bs.[BasketId] = @basketId",
               param: new { basketId }
           );
        }
        public int Remove(string key)
        {
            throw new System.NotImplementedException();
        }
        public int Update(Basket entity)
        {
            throw new System.NotImplementedException();
        }
        public int UpdateActive(int basketId, int active)
        {
            return Execute(
                          sql: @"update Baskets set [IsActive] = @active where BasketId = @basketId",
                          param: new { active, basketId }
                      );
        }
        public int UpdateAddress(int basketId, int addressId)
        {
            return Execute(
                           sql: @"update Baskets set AddressId = @addressId where BasketId = @basketId",
                           param: new { addressId, basketId }
                       );
        }
        public int UpdateBasket(BasketViewModel basketViewModel)
        {
            return ExecuteScalar<int>(
                          sql: @"update Baskets set [LastUpdate] = @lastUpdate, [TotalAmount] = @totalAmount, [TotalVat] = @totalVat, [SubTotal] = @subTotal where BasketId = @basketId",
                          param: new
                          {
                              lastUpdate = basketViewModel.LastUpdate,                          
                              totalAmount = basketViewModel.TotalAmount,                           
                              totalVat = basketViewModel.TotalVat,                            
                              subTotal = basketViewModel.SubTotal,
                              basketId = basketViewModel.BasketId
                          }
                      );
        }
        public int UpdatePayType(int basketId, int payTypeId)
        {
            return Execute(
                           sql: @"update Baskets set PayTypeId = @payTypeId where BasketId = @basketId",
                           param: new { payTypeId, basketId }
                       );
        }
        public int UpdateState(int basketId, int stateId)
        {
            return Execute(
                           sql: @"update Baskets set StateId = @stateId where BasketId = @basketId",
                           param: new { stateId, basketId }
                       );
        }
    }
}
