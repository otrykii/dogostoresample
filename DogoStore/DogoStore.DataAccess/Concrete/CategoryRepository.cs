﻿using DogoStore.DataAccess.Abstract;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Category;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace DogoStore.DataAccess.Concrete
{
    public class CategoryRepository : RepositoryBase, ICategoryRepository
    {

        public CategoryRepository(IDbTransaction transaction)
            : base(transaction)
        {


        }

        public int Add(Category entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> All()
        {
            return Query<Category>(
                sql: "SELECT * FROM Category IsDelete = 0"
            );
        }

        public Category Find(int key)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SelectCategoryViewModel> ListSelectCategory()
        {
            return Query<SelectCategoryViewModel>(
                sql: @"SELECT [CategoryId] as ProductCategoryId,[CategoryName] FROM [Categories] where IsDelete = 0"
            );
        }

        public int Remove(int key)
        {
            throw new NotImplementedException();
        }

        public int Update(Category entity)
        {
            throw new NotImplementedException();
        }
    }
}
