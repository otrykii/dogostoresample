﻿using DogoStore.Core.DataAccess;
using DogoStore.Entities.Concrete;
using System.Collections.Generic;
using DogoStore.Entities.ViewModel.Product;

namespace DogoStore.DataAccess.Abstract
{
    public interface IProductRepository : IRepository<Product, int>
    {
        IEnumerable<ProductViewModel> ListProducts();

        IEnumerable<ProductViewModel> ListByCategoryProducts(int page,int categoryId);

        ProductViewModel GetProduct(int productId);

        IEnumerable<ProductViewModel> HomePage();

    }
}
