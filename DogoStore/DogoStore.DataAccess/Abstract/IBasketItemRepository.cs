﻿using DogoStore.Core.DataAccess;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Basket;
using System.Collections.Generic;

namespace DogoStore.DataAccess.Abstract
{
    public interface IBasketItemRepository : IRepository<BasketItem, int>
    {
        BasketViewModel GetActiveBasket(string userId, int isActive, int stateId);

        IEnumerable<BasketItemViewModel> GetListBasketItems(int basketId);

        IEnumerable<BasketItem> GetBasketItems(int basketId);

        int ChangeBasketItem(BasketItemViewModel basketViewModel);

        int GetActiveBasketId(string userId, int isActive, int stateId);

        BasketItem IsSameProduct(int basketId, int productId);
    }
}
