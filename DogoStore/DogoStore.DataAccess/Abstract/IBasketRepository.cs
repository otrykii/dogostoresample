﻿using DogoStore.Core.DataAccess;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Basket;

namespace DogoStore.DataAccess.Abstract
{
    public interface IBasketRepository : IRepository<Basket, string>
    {
        BasketViewModel GetBasket(int basketId);

        int CreateBasket(BasketViewModel entity);

        int UpdateBasket(BasketViewModel basketViewModel);

        int UpdateAddress(int basketId, int addressId);

        int UpdatePayType(int basketId, int payTypeId);

        int UpdateState(int basketId, int stateId);

        int UpdateActive(int basketId, int active);

    }
}
