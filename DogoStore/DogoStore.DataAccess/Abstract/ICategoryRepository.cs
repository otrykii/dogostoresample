﻿using DogoStore.Core.DataAccess;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.DataAccess.Abstract
{
    public interface ICategoryRepository : IRepository<Category, int>
    {
        IEnumerable<SelectCategoryViewModel> ListSelectCategory();

    }
}
