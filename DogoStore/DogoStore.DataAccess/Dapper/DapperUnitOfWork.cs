﻿using DogoStore.DataAccess.Abstract;
using DogoStore.DataAccess.Concrete;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DogoStore.DataAccess.Dapper
{
    public class DapperUnitOfWork : IUnitOfWork
    {

        #region Fields
        private IDbConnection _connection;
        private IDbTransaction _transaction;

        private IProductRepository _productRepository;
        private ICategoryRepository _categoryRepository;
        private IBasketRepository _basketRepository;
        private IBasketItemRepository _basketItemRepository;
        
        private bool _disposed;
        #endregion

        public DapperUnitOfWork(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
            _connection.Open();
            _transaction = _connection.BeginTransaction();
        }


        #region IUnitOfWork Members
        public IProductRepository ProductRepository
        {
            get
            {
                return _productRepository
                    ?? (_productRepository = new ProductRepository(_transaction));
            }
        }
        public ICategoryRepository CategoryRepository
        {
            get
            {
                return _categoryRepository
                    ?? (_categoryRepository = new CategoryRepository(_transaction));
            }
        }
        public IBasketRepository BasketRepository
        {
            get
            {
                return _basketRepository
                    ?? (_basketRepository = new BasketRepository(_transaction));
            }
        }
        public IBasketItemRepository BasketItemRepository
        {
            get
            {
                return _basketItemRepository
                    ?? (_basketItemRepository = new BasketItemRepository(_transaction));
            }
        }

        public void Commit()
        {
            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
            }
            finally
            {
                _transaction.Dispose();
                resetRepositories();
                _transaction = _connection.BeginTransaction();
            }
        }
        public void Dispose()
        {
            dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

        #region Private Methods
        private void resetRepositories()
        {
            _productRepository = null;
            _categoryRepository = null;
            _basketItemRepository = null;
            _basketRepository = null;

        }
        private void dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    if (_transaction != null)
                    {
                        _transaction.Dispose();
                        _transaction = null;
                    }
                    if (_connection != null)
                    {
                        _connection.Dispose();
                        _connection = null;
                    }
                }
                _disposed = true;
            }
        }

        ~DapperUnitOfWork()
        {
            dispose(false);
        }
        #endregion
    }
}
