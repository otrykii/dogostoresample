﻿using DogoStore.DataAccess.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        IProductRepository ProductRepository { get; }
        ICategoryRepository CategoryRepository { get; }
        IBasketRepository BasketRepository { get; }
        IBasketItemRepository BasketItemRepository { get; }
        void Commit();
    }
}
