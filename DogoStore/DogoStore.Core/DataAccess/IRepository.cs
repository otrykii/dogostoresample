﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Core.DataAccess
{
    public interface IRepository<TEntity, TKey> where TEntity : class
    {
        IEnumerable<TEntity> All();
        TEntity Find(TKey key);
        int Add(TEntity entity);
        int Update(TEntity entity);
        int Remove(TKey key);

    }
}
