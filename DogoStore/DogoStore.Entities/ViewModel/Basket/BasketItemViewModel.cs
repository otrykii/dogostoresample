﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Entities.ViewModel.Basket
{
    public class BasketItemViewModel
    {
        public int BasketItemId { get; set; }

        public int BasketId { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public decimal Price { get; set; }

        public decimal Vat { get; set; }

        public DateTime CreateTime { get; set; }

        public int Amount { get; set; }

        public decimal TotalPrice { get; set; }
    }

}
