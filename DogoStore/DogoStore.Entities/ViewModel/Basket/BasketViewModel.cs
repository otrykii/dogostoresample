﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Entities.ViewModel.Basket
{
    public class BasketViewModel
    {
        public int BasketId { get; set; }

        public string UserId { get; set; }

        public int PayTypeId { get; set; }

        public int AddressId { get; set; }

        public int StateId { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal TotalVat { get; set; }

        public decimal SubTotal { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime LastUpdate { get; set; }

        public bool IsActive { get; set; }

        public string UserName { get; set; }

        public string PayTypeName { get; set; }

        public string StateName { get; set; }

        public IEnumerable<BasketItemViewModel> BasketItemViewModels { get; set; }

    }
}
