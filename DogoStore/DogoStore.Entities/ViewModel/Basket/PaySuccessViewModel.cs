﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Entities.ViewModel.Basket
{
    public class PaySuccessViewModel
    {
        public int BasketId { get; set; }

        public string UserName { get; set; }

        public string PayTypeName { get; set; }
    }
}
