﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Entities.ViewModel.Category
{
    public class SelectCategoryViewModel
    {
        public int ProductCategoryId { get; set; }
        public string CategoryName { get; set; }

    }
}
