﻿using DogoStore.Entities.ViewModel.Category;
using System;
using System.Collections.Generic;

namespace DogoStore.Entities.ViewModel.Product
{
    public class ProductViewModel
    {
       public int ProductId { get; set; }

        public int ProductCategoryId { get; set; }

        public string ProductName { get; set; }

        public decimal ProductPrice { get; set; }

        public int ProductStock { get; set; }

        public DateTime CreateDate { get; set; }

        public string ProductStockCode { get; set; }

        public string CategoryName { get; set; }

        public bool IsDelete { get; set; }

        public bool IsHome { get; set; }

        public int TotalCount { get; set; }
        public IEnumerable<SelectCategoryViewModel> Categories { set; get; }
        public SelectCategoryViewModel SelectedCategory { set; get; }

    }
}
