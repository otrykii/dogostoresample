﻿using System.ComponentModel.DataAnnotations;

namespace DogoStore.Entities.ViewModel.User
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage ="Kullanıcı Adı Boş olamaz")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Email Boş olamaz")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Şifre Boş olamaz")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Doğrulama Boş olamaz")]
        [Compare("Password")]
        [DataType(DataType.Password)]
        public string ConfirmedPassword { get; set; }
    }
}