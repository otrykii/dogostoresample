﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace DogoStore.Entities.ViewModel.User
{
    public class LoginViewModel
    {

        [Required(ErrorMessage = "Kullanıcı adı boş geçilemez.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Kullanıcı şifre boş geçilemez.")]
        public string Password { get; set; }
    }
}
