﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Entities.Params.Basket
{
    public class AddToBasketParam
    {
        public int BasketId { get; set; }
        public string ActiveUserId { get; set; }
        public int ProductId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }

    }
}
