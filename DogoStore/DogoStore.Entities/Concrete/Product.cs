﻿using DogoStore.Core.Entities;
using System;

namespace DogoStore.Entities.Concrete
{
    public class Product : IEntity
    {
        public int ProductId { get; set; }

        public int ProductCategoryId { get; set; }

        public string ProductName { get; set; }

        public decimal ProductPrice { get; set; }

        public int ProductStock { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsDelete { get; set; }
        public string ProductStockCode { get; set; }

        public bool IsHome { get; set; }

    }
}
