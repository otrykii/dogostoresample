﻿using DogoStore.Core.Entities;

namespace DogoStore.Entities.Concrete
{
    public class Address : IEntity
    {
        public int AddressId { get; set; }

        public int UserId { get; set; }

        public string AddressName { get; set; }

        public string AddressDesc { get; set; }

        public string AddressDetail { get; set; }

        public string AddressCity { get; set; }

        public string AddressCountry { get; set; }

        public string AddressCounty { get; set; }

        public bool IsDelete { get; set; }

    }
}
