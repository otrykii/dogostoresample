﻿using DogoStore.Core.Entities;
using System;

namespace DogoStore.Entities.Concrete
{
    public class BasketItem : IEntity
    {
        public int BasketItemId { get; set; }

        public int BasketId { get; set; }

        public int ProductId { get; set; }

        public decimal Price { get; set; }

        public decimal Vat { get; set; }

        public DateTime CreateTime { get; set; }

        public int Amount { get; set; }

        public decimal TotalPrice { get; set; }

    }
}
