﻿using DogoStore.Core.Entities;
using System;
using System.Collections.Generic;

namespace DogoStore.Entities.Concrete
{
    public class Category : IEntity
    {

        public int CategoryId { get; set; }

        public string CategoryName { get; set; }

        public int CategorySort { get; set; }

        public string CategoryUrl { get; set; }

        public DateTime CreateDate { get; set; }

        public bool IsDelete { get; set; }

    }
}
