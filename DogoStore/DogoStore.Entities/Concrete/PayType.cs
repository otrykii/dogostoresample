﻿using DogoStore.Core.Entities;

namespace DogoStore.Entities.Concrete
{
    public class PayType : IEntity
    {
        public int PayTypeId { get; set; }

        public string PayTypeName { get; set; }

        public bool IsDelete { get; set; }

    }
}
