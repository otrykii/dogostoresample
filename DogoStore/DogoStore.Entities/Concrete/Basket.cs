﻿using DogoStore.Core.Entities;
using System;

namespace DogoStore.Entities.Concrete
{
    public class Basket : IEntity
    {
        public int BasketId { get; set; }

        public int UserId { get; set; }

        public int PayTypeId { get; set; }

        public int AddressId { get; set; }

        public int StateId { get; set; }

        public decimal TotalAmount { get; set; }

        public decimal TotalVat { get; set; }

        public decimal SubTotal { get; set; }

        public DateTime OrderDate { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime LastUpdate { get; set; }

        public bool IsActive { get; set; }

    }
}
