﻿using DogoStore.Core.Entities;
using System.Collections.Generic;

namespace DogoStore.Entities.Concrete
{
    public class BasketState : IEntity
    {
        public int StateId { get; set; }

        public string StateName { get; set; }

        public bool IsDelete { get; set; }


    }
}
