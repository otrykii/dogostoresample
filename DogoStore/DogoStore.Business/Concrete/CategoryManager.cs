﻿using DogoStore.Business.Abstract;
using DogoStore.DataAccess;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Category;
using DogoStore.Entities.ViewModel.Product;
using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Business.Concrete
{
    public class CategoryManager : ICategoryService
    {
        private readonly IUnitOfWork _uow;

        public CategoryManager(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public IEnumerable<Category> ListAllCategories()
        {
            return _uow.CategoryRepository.All();
        }

        public IEnumerable<SelectCategoryViewModel> ListSelectCategories()
        {
            return _uow.CategoryRepository.ListSelectCategory();
        }
    }
}
