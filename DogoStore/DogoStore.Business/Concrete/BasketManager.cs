﻿using DogoStore.Business.Abstract;
using DogoStore.Business.Helper.Enum;
using DogoStore.DataAccess;
using DogoStore.Entities.ViewModel.Basket;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;

namespace DogoStore.Business.Concrete
{
    public class BasketManager : IBasketService
    {
        private readonly IUnitOfWork _uow;

        public BasketManager(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public int CreateBasket(BasketViewModel basketViewModel)
        {
            return _uow.BasketRepository.CreateBasket(basketViewModel);
        }

        public int CreateBasketDefault(string userId, int productId, int quantity, decimal price)
        {
            int basketId = 0;

            decimal total = price * quantity;
            decimal subTotal = (total) - Helper.TaxCalculation.Calculation(price, quantity);
            decimal totalVat = Helper.TaxCalculation.Calculation(price, quantity);

            int InsertResult = CreateBasket(new BasketViewModel
            {
                UserId = userId,
                IsActive = Convert.ToBoolean(BasketActive.Active),
                StateId = (int)BasketEnum.InProggress,
                SubTotal = subTotal,
                CreateDate = DateTime.Now,
                LastUpdate = DateTime.Now,
                AddressId = 0,
                PayTypeId = 0,
                TotalAmount = total,
                TotalVat = totalVat
            });

            if (InsertResult <= 0)
                return basketId;

            _uow.Commit();

            basketId = InsertResult;

            return basketId;
        }

        public BasketViewModel GetActiveBasket(int userId, int isActive, int stateId)
        {
            throw new System.NotImplementedException();
        }

        public BasketViewModel GetBasketAndItems(int basketId)
        {
            BasketViewModel basketViewModel = _uow.BasketRepository.GetBasket(basketId);
            basketViewModel.BasketItemViewModels = _uow.BasketItemRepository.GetListBasketItems(basketId);

            return basketViewModel;
        }

        public int UpdateActive(int basketId, int active)
        {
            return _uow.BasketRepository.UpdatePayType(basketId, active);
        }

        public int UpdateAddress(int basketId, int addressId)
        {
            return _uow.BasketRepository.UpdatePayType(basketId, addressId);
        }

        public int UpdateBasket(BasketViewModel basketViewModel)
        {
            return _uow.BasketRepository.UpdateBasket(basketViewModel);
        }

        public int UpdatePayType(int basketId, int payTypeId)
        {
            return _uow.BasketRepository.UpdatePayType(basketId, payTypeId);
        }

        public int UpdateState(int basketId, int stateId)
        {
            return _uow.BasketRepository.UpdateState(basketId, stateId);
        }
    }
}
