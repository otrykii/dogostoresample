﻿using DogoStore.Business.Abstract;
using DogoStore.Business.ValidationRules.FluentValidation;
using DogoStore.DataAccess;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Product;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DogoStore.Business.Concrete
{
    public class ProductManager : IProductService
    {
        private readonly IUnitOfWork _uow;

        public ProductManager(IUnitOfWork uow)
        {
            _uow = uow;
        }

        public bool Add(ProductViewModel product)
        {
            bool ReturnResult = false;

            ProductValidator validationRules = new ProductValidator();
            var result = validationRules.Validate(product);

            if (!result.IsValid)
                throw new ValidationException(result.Errors);

            int product1 = _uow.ProductRepository.Add(
                new Product()
                {
                    CreateDate = DateTime.Now,
                    IsDelete = false,
                    IsHome = product.IsHome,
                    ProductCategoryId = product.ProductCategoryId,
                    ProductName = product.ProductName,
                    ProductPrice = product.ProductPrice,
                    ProductStock = product.ProductStock,
                    ProductStockCode = product.ProductStockCode
                }
                );

            if (product1 > 0)
            {
                ReturnResult = true;
                _uow.Commit();
            }

            return ReturnResult;
        }

        public bool Edit(ProductViewModel product)
        {
            bool ReturnResult = false;

            ProductValidator validationRules = new ProductValidator();
            var result = validationRules.Validate(product);

            if (!result.IsValid)
                throw new ValidationException(result.Errors);

            int product1 = _uow.ProductRepository.Update(
                new Product()
                {
                    ProductId = product.ProductId,
                    IsHome = product.IsHome,
                    ProductCategoryId = product.ProductCategoryId,
                    ProductName = product.ProductName,
                    ProductPrice = product.ProductPrice,
                    ProductStock = product.ProductStock,
                    ProductStockCode = product.ProductStockCode
                });

            if (product1 > 0)
            {
                ReturnResult = true;
                _uow.Commit();
            }

            return ReturnResult;
        }

        public bool Remove(int Id)
        {
            bool result = false;

            if (_uow.ProductRepository.Remove(Id) > 0)
            {
                result = true;
                _uow.Commit();
            }

            return result;
        }

        public ProductViewModel GetProduct(int ProductId)
        {
            return _uow.ProductRepository.GetProduct(ProductId);
        }

        public IEnumerable<ProductViewModel> ListHomePageProduct(int take)
        {
            return _uow.ProductRepository.HomePage().Take(take);
        }

        IEnumerable<ProductViewModel> IProductService.ListProductsAll()
        {
            return _uow.ProductRepository.ListProducts();
        }

    }
}
