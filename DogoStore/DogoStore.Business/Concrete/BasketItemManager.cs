﻿using DogoStore.Business.Abstract;
using DogoStore.Business.Helper.Enum;
using DogoStore.DataAccess;
using DogoStore.Entities.Concrete;
using DogoStore.Entities.Params.Basket;
using DogoStore.Entities.ViewModel.Basket;
using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Business.Concrete
{
    public class BasketItemManager : IBasketItemService
    {
        private readonly IUnitOfWork _uow;
        public BasketItemManager(IUnitOfWork uow)
        {
            _uow = uow;
        }
        private BasketItem IsCheckSameProduct(int basketId, int productId)
        {
            return _uow.BasketItemRepository.IsSameProduct(basketId, productId);
        }
        public bool UpdateBasketWidthBasketItems(int basketId)
        {
            bool result = false;

            decimal totalVat = 0;
            decimal tubTotal = 0;
            decimal totalAmount = 0;
            decimal subTotal = 0;

            foreach (var item in _uow.BasketItemRepository.GetBasketItems(basketId))
                totalAmount += item.TotalPrice;

            totalVat = Helper.TaxCalculation.Calculation(totalAmount);
            subTotal = totalAmount - totalVat;

            int updateResult = _uow.BasketRepository.UpdateBasket(new BasketViewModel
            {
                BasketId = basketId,
                SubTotal = subTotal,
                TotalVat = totalVat,
                TotalAmount = totalAmount,
                LastUpdate = DateTime.Now
            });

            if (updateResult != -1)
                result = true;

            _uow.Commit();

            return result;
        }
        public bool AddToBasketWithId(AddToBasketParam addToBasketParam)
        {
            bool result = false;

            var basketItem = IsCheckSameProduct(addToBasketParam.BasketId, addToBasketParam.ProductId);
            decimal totalAmount = addToBasketParam.Price * addToBasketParam.Quantity;
            decimal vatAmount = Helper.TaxCalculation.Calculation(addToBasketParam.Price, addToBasketParam.Quantity);

            if (basketItem != null)
            {
                totalAmount = addToBasketParam.Price * (addToBasketParam.Quantity + basketItem.Amount);
                vatAmount = Helper.TaxCalculation.Calculation(addToBasketParam.Price, (addToBasketParam.Quantity + basketItem.Amount));

                int insertResult = _uow.BasketItemRepository.ChangeBasketItem(new BasketItemViewModel()
                {
                    Amount = (addToBasketParam.Quantity + basketItem.Amount),
                    BasketItemId = basketItem.BasketItemId,
                    BasketId = addToBasketParam.BasketId,
                    Price = addToBasketParam.Price,
                    TotalPrice = totalAmount,
                    Vat = vatAmount
                });

                if (insertResult > 0)
                    result = true;
            }
            else
            {
                int insertResult = _uow.BasketItemRepository.Add(new BasketItem
                {
                    Amount = addToBasketParam.Quantity,
                    CreateTime = DateTime.Now,
                    BasketId = addToBasketParam.BasketId,
                    Price = addToBasketParam.Price,
                    ProductId = addToBasketParam.ProductId,
                    Vat = vatAmount,
                    TotalPrice = totalAmount
                });

                if (insertResult > 0)
                    result = true;
            }

            _uow.Commit();

            if (result)
                UpdateBasketWidthBasketItems(addToBasketParam.BasketId);

            return result;
        }
        public bool ChangeBasketItemAmount(int basketId, int basketItemId, string userId, int prdocutId, int quantity)
        {
            throw new NotImplementedException();
        }
        public int GetActiveBasketId(string userId, int isActive, int stateId)
        {
            throw new NotImplementedException();
        }
        public int GetActiveBasketOrCreateBasket(string userId, int isActive, int stateId)
        {
            int basketId = 0;

            basketId = _uow.BasketItemRepository.GetActiveBasketId(userId, isActive, stateId);

            return basketId;
        }
        public IEnumerable<BasketItemViewModel> GetBasketListItems(int basketId)
        {
            return _uow.BasketItemRepository.GetListBasketItems(basketId);
        }
        public bool RemoveBasketItem(int basketItemId)
        {
            bool result = false;

            if (_uow.BasketItemRepository.Remove(basketItemId) != -1)
                result = true;

            return result;
        }
    }
}
