﻿using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Product;
using System.Collections.Generic;

namespace DogoStore.Business.Abstract
{
    public interface IProductService
    {
        IEnumerable<ProductViewModel> ListProductsAll();
        IEnumerable<ProductViewModel> ListHomePageProduct(int take);
        ProductViewModel GetProduct(int ProductId);
        bool Add(ProductViewModel product);
        bool Edit(ProductViewModel product);
        bool Remove(int Id);

    }
}
