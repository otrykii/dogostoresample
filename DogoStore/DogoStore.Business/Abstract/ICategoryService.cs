﻿using DogoStore.Entities.Concrete;
using DogoStore.Entities.ViewModel.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Business.Abstract
{
    public interface ICategoryService
    {
        IEnumerable<Category> ListAllCategories();

        IEnumerable<SelectCategoryViewModel> ListSelectCategories();

    }
}
