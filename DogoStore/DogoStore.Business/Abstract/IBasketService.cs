﻿using DogoStore.Entities.ViewModel.Basket;
using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Business.Abstract
{
    public interface IBasketService
    {
        BasketViewModel GetActiveBasket(int userId, int isActive, int stateId);
        BasketViewModel GetBasketAndItems(int basketId);
        int CreateBasket(BasketViewModel basketViewModel);

        int UpdateActive(int basketId, int active);

        int UpdateAddress(int basketId, int addressId);

        int UpdateBasket(BasketViewModel basketViewModel);

        int UpdatePayType(int basketId, int payTypeId);

        int UpdateState(int basketId, int stateId);
        int CreateBasketDefault(string userId, int productId, int quantity, decimal price);
    }
}

