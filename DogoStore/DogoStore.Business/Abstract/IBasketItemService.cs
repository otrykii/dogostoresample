﻿using DogoStore.Entities.Params.Basket;
using DogoStore.Entities.ViewModel.Basket;
using System.Collections.Generic;

namespace DogoStore.Business.Abstract
{
    public interface IBasketItemService
    {
        int GetActiveBasketId(string userId, int isActive, int stateId);
        bool AddToBasketWithId(AddToBasketParam addTobasketParam);
        bool ChangeBasketItemAmount(int basketId, int basketItemId, string userId, int prdocutId, int quantity);
        IEnumerable<BasketItemViewModel> GetBasketListItems(int basketıd);
        bool UpdateBasketWidthBasketItems(int basketId);
        bool RemoveBasketItem(int basketItemId);

    }
}
