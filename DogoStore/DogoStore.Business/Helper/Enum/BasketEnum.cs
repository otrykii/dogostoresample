﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Business.Helper.Enum
{
    public enum BasketEnum
    {
        InProggress = 1,
        Waiting = 2,
        Approved = 3,
        Cancel = 4,
    }

    public enum BasketActive
    {
        Active = 1,
        NotActive = 0
    }
}
