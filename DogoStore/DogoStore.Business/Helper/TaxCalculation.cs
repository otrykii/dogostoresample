﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Business.Helper
{
    public static class TaxCalculation
    {
        public static decimal Calculation(decimal price, int quantity)
        {
            int taxRate = 18;
            decimal total = price * quantity;

            return total * taxRate / 100;
        }

        public static decimal Calculation(decimal price)
        {

            decimal res = price * 18 / 100;
            return res;
        }

    }
}
