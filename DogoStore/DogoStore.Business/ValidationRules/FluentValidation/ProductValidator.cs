﻿using DogoStore.Entities.ViewModel.Product;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace DogoStore.Business.ValidationRules.FluentValidation
{
    public class ProductValidator : AbstractValidator<ProductViewModel>
    {
        public ProductValidator()
        {
            RuleFor(r => r.ProductName).NotEmpty().WithMessage("Ürün ismi gereklidir.");
            RuleFor(r => r.ProductPrice).NotNull().WithMessage("Ürün fiyat gereklidir.");
            RuleFor(r => r.ProductCategoryId).NotNull().WithMessage("Ürün kategori gereklidir.");
            RuleFor(r => r.ProductStock).NotNull().WithMessage("Ürün stok gereklidir.");
            RuleFor(r => r.ProductStockCode).NotEmpty().WithMessage("Ürün stok kodu gereklidir.");

        }
    }
}
