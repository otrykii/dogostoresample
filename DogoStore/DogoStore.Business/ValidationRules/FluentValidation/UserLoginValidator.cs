﻿using DogoStore.Entities.ViewModel.User;
using FluentValidation;

namespace DogoStore.Business.ValidationRules.FluentValidation
{
    public class UserLoginValidator : AbstractValidator<LoginViewModel>
    {
        public UserLoginValidator()
        {
            RuleFor(r => r.Password).NotEmpty().WithMessage("Şifre boş olamaz..!");
            RuleFor(r => r.UserName).NotEmpty().WithMessage("Kullanıcı adı boş olamaz..!");

        }
    }
}
